﻿// Lab8-Umbre.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
// Lab8 - Shadow mapping.cpp : Defines the entry point for the console application.
//

#include <stdlib.h> // necesare pentru citirea shaderStencilTesting-elor
#include <stdio.h>
#include <math.h> 
#include <vector> 

#include <GL/glew.h>

#define GLM_FORCE_CTOR_INIT 
#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

//// GL includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"

#include <iostream>
#include <fstream>
#include <sstream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>



#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")
#pragma comment (lib, "assimp-vc140-mt.lib")

using namespace std;

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

Camera camera(glm::vec3(0.0f, 15.0f, 0.0f));

GLint TextureFromFile(const char* path, string directory);

//void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);

void renderScene(const Shader& shader);
void renderFloor();

// timing
double deltaTime = 0.0f;    // time between current frame and last frame
double lastFrame = 0.0f;

bool RotateLight = false;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_L && action == GLFW_PRESS)
	{
		RotateLight = true;
	}
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
	{
		RotateLight = false;
	}
}

void RotateGrass(Shader& shaderBlending, glm::mat4& model)
{
	for (int i = 1; i < 10; i++)
	{
		glm::vec3 Position(0, 1.0f, 0);
		model = glm::rotate(model, glm::radians(i * 30.f), Position);
		shaderBlending.SetMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
}

unsigned int loadCubemap(std::vector<std::string> faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrComponents;
	for (unsigned int i = 0; i < faces.size(); i++) {
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrComponents, 0);
		if (data) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		else {
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}

GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

//Models declarations
Model tankModel;
Model helicopModel;
Model hangar;
Model heliModel2;

Model WWII_Tank_Germany;
Model Ozelot_tank;

Model truckModel;

Model towerModel_Wood;
Model Radio;
Model Prison_Watchtower;

Model RoadBlock;

Model fenceGate;
Model fence;

int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}

	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Tancodrom", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	//glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetKeyCallback(window, key_callback);

	// tell GLFW to capture our mouse
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewInit();

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// build and compile shaders
	// -------------------------
	Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");
	Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");
	Shader shaderSkybox("skybox.vs", "skybox.fs");
	Shader shaderCubeMap("cube.vs", "cube.fs");
	Shader shader("modelLoading.vs", "modelLoading.frag");
	Shader shaderBlending("Blending.vs", "Blending.fs");

	// Load models
	std::string path = strExePath + "\\..\\..\\Tancodrom\\obj_models\\Tanc\\Abrams_BF3.obj";
	const char* tank = path.c_str();
	tankModel.Init((GLchar*)tank);

	std::string path2 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\1\\OH-58D.obj";
	const char* heli = path2.c_str();
	helicopModel.Init((GLchar*)heli);

	std::string path3 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\2\\Shelter_simple.obj";
	const char* han = path3.c_str();
	hangar.Init((GLchar*)han);

	std::string path4 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\4\\ah64d.obj";
	const char* heli2 = path4.c_str();
	heliModel2.Init((GLchar*)heli2);

	std::string path6 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\6\\14077_WWII_Tank_Germany_Panzer_III_v1_L2.obj";
	const char* tank2 = path6.c_str();
	WWII_Tank_Germany.Init((GLchar*)tank2);

	std::string path7 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\7\\Ozelot.obj";
	const char* tank3 = path7.c_str();
	Ozelot_tank.Init((GLchar*)tank3);

	std::string path8 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\8\\Surv_Bus_ready.obj";
	const char* truck = path8.c_str();
	truckModel.Init((GLchar*)truck);

	std::string path9 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\9\\wooden watch tower2.obj";
	const char* tower = path9.c_str();
	towerModel_Wood.Init((GLchar*)tower);

	std::string path10 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\10\\tower.obj";
	const char* tower2 = path10.c_str();
	Radio.Init((GLchar*)tower2);

	std::string path11 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\11\\RoadBlockade_01.obj";
	const char* block = path11.c_str();
	RoadBlock.Init((GLchar*)block);

	std::string path12 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\12\\Prison_Watchtower_textured.obj";
	const char* tower3 = path12.c_str();
	Prison_Watchtower.Init((GLchar*)tower3);

	std::string path13 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\13\\military fence gate.obj";
	const char* fence2 = path13.c_str();
	fenceGate.Init((GLchar*)fence2);

	std::string path15 = strExePath + "\\..\\..\\Tancodrom\\obj_models\\15\\fance.obj";
	const char* fence3 = path15.c_str();
	fence.Init((GLchar*)fence3);


	// ** SKYBOX **

	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------
	float cubeVertices[] = {
		// positions          // normals
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
	};
	float skyboxVertices[] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f, -1.0f,
		 1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f, -1.0f,
		 1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f
	};

	// cube VAO
	unsigned int cubeMapVAO, cubeMapVBO;
	glGenVertexArrays(1, &cubeMapVAO);
	glGenBuffers(1, &cubeMapVBO);
	glBindVertexArray(cubeMapVAO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeMapVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	// skybox VAO
	unsigned int skyboxVAO, skyboxVBO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	// load textures
	// -------------
	std::string pathToSkybox = strExePath + "\\..\\..\\Tancodrom\\skybox\\Clouds\\";
	std::vector<std::string> faces
	{
		pathToSkybox + "front.tga",
		pathToSkybox + "back.tga",
		pathToSkybox + "top.tga",
		pathToSkybox + "bottom.tga",
		pathToSkybox + "right.tga",
		pathToSkybox + "left.tga",
	};
	unsigned int cubemapTexture = loadCubemap(faces);

	// shader configuration
	// --------------------
	//

	shaderCubeMap.Use();
	shaderCubeMap.SetInt("skybox", 0);

	shaderSkybox.Use();
	shaderSkybox.SetInt("skybox", 0);


	shaderCubeMap.Use();
	shaderCubeMap.SetInt("skybox", 0);

	shaderSkybox.Use();
	shaderSkybox.SetInt("skybox", 0);

	// Grass vertices
	float grassVertices[] = {
		// positions          // texture Coords 
		-0.5f, 0.5f,  0.0f,  0.0f, 0.0f,
		-0.5f, -0.5f,  0.0f,  0.0f, 1.0f,
		0.5f, -0.5f, 0.0f,  1.0f, 1.0f,

		-0.5f, 0.5f, 0.0f,  0.0f, 0.0f,
		0.5f, -0.5f, 0.0f,  1.0f, 1.0f,
		0.5f, 0.5f, 0.0f,  1.0f, 0.0f
	};

	// Grass VAO si VBO
	unsigned int grassVAO, grassVBO;
	glGenVertexArrays(1, &grassVAO);
	glGenBuffers(1, &grassVBO);
	glBindVertexArray(grassVAO);
	glBindBuffer(GL_ARRAY_BUFFER, grassVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(grassVertices), &grassVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	// load textures
	// -------------
	unsigned int floorTexture = TextureFromFile("\\..\\..\\Tancodrom\\skybox\\bottom.tga", strExePath);
	// Grass texture
	unsigned int grassTexture = TextureFromFile("\\..\\..\\Tancodrom\\textures\\grass3.png", strExePath);


	// configure depth map FBO
	// -----------------------
	const unsigned int SHADOW_WIDTH = 4096, SHADOW_HEIGHT = 4096;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// shader configuration
	// --------------------
	shadowMappingShader.Use();
	shadowMappingShader.SetInt("diffuseTexture", 0);
	shadowMappingShader.SetInt("shadowMap", 1);

	// lighting info
	// -------------
	glm::vec3 lightPos(-2.0f, 4.0f, -1.0f);

	glEnable(GL_CULL_FACE);

	glm::mat4 projection = glm::perspective(camera.GetZoom(), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);

	// render loop
	// -----------
	float currentFrame;
	glm::mat4 model;
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		if (RotateLight)
		{
			lightPos.x = cos(currentFrame);
			lightPos.z = sin(currentFrame);
		}
		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// 1. render depth of scene to texture (from light's perspective)
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		float near_plane = 1.0f, far_plane = 7.5f;
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;

		// render scene from light's point of view
		shadowMappingDepthShader.Use();
		shadowMappingDepthShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		renderScene(shadowMappingDepthShader);
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// reset viewport
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// 2. render scene as normal using the generated depth/shadow map 
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shadowMappingShader.Use();
		glm::mat4 view = camera.GetViewMatrix();
		shadowMappingShader.SetMat4("projection", projection);
		shadowMappingShader.SetMat4("view", view);

		// set light uniforms
		shadowMappingShader.SetVec3("viewPos", camera.GetPosition());
		shadowMappingShader.SetVec3("lightPos", lightPos);
		shadowMappingShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glDisable(GL_CULL_FACE);
		renderScene(shadowMappingShader);

		// draw skybox as last
		glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
		shaderSkybox.Use();
		view = glm::mat4(glm::mat3(camera.GetViewMatrix())); // remove translation from the view matrix
		shaderSkybox.SetMat4("view", view);
		shaderSkybox.SetMat4("projection", projection);

		// skybox cube
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // set depth function back to default

		//draw objects
		shader.Use();
		view = camera.GetViewMatrix();
		shader.SetMat4("view", view);
		shader.SetMat4("projection", projection);
		renderScene(shader);


		shaderBlending.Use();
		shaderBlending.SetInt("texture1", 0);
		shaderBlending.SetMat4("projection", projection);
		shaderBlending.SetMat4("view", view);

		// Draw vegetation
		glBindVertexArray(grassVAO);
		glBindTexture(GL_TEXTURE_2D, grassTexture);
		model = glm::mat4();
		shaderBlending.SetMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		//for (int i = 25; i > -25; i--)
		{
			for (int j = 20; j > -24; j--)
			{
				if (j > 4 || j < 0)
				{
					model = glm::mat4();
					glm::vec3 Position(-0.5f + j, 0.5f, 3.5f);
					model = glm::translate(model, Position);
					shaderBlending.SetMat4("model", model);
					glDrawArrays(GL_TRIANGLES, 0, 6);
					RotateGrass(shaderBlending, model);
				}
			}
		}

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	glfwTerminate();
	return 0;
}

// renders the 3D scene
// --------------------
void renderScene(const Shader& shader)
{
	// floor
	glm::mat4 model;
	shader.SetMat4("model", model);
	renderFloor();



	model = glm::mat4();
	model = glm::translate(model, glm::vec3(0.0f, 0.8f, -16.5f)); // Translate it down a bit so it's at the center of the scene
	model = glm::rotate(model, 660.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	tankModel.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-15.0f, 0.8f, -16.5f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 660.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	tankModel.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(16.0f, -2.7f, 17.0f)); // Translate it down a bit so it's at the center of the scene
	model = glm::rotate(model, 3.14f / 2.0f, glm::vec3(-2.7f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(3.0f, 3.0f, 3.0f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	WWII_Tank_Germany.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(15.0f, 1.5f, -19.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.4f, 1.4f, 1.4f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	helicopModel.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-15.0f, 0.0f, -15.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	hangar.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(17.0f, 0.0f, 17.0f)); // Translate it down a bit so it's at the center of the scene
	model = glm::rotate(model, 3.14f / 2.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	hangar.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, -15.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	hangar.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(1.0f, -1.5f, 18.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	heliModel2.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(20.0f, -2.7f, -1.0f)); // Translate it down a bit so it's at the center of the scene
	model = glm::rotate(model, 3.14f / 2.0f, glm::vec3(-2.7f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(3.0f, 3.0f, 3.0f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	WWII_Tank_Germany.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(10.0f, 0.0f, -3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	Ozelot_tank.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-20.0f, 1.7f, 14.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.8f, 1.8f, 1.8f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	truckModel.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(15.0f, -0.8f, 6.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	towerModel_Wood.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-10.0f, 0.0f, 20.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(3.0f, 3.0f, 3.0f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	Radio.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(20.0f, 0.0f, -10.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.8f, 1.8f, 1.8f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	RoadBlock.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(9.5f, 0.0f, -10.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.8f, 1.8f, 1.8f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	RoadBlock.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-13.0f, 0.0f, 0.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.8f, 1.8f, 1.8f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	RoadBlock.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-10.0f, 0.0f, 9.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(1.8f, 1.8f, 1.8f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	RoadBlock.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-20.0f, 0.0f, 0.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.6f, 0.6f, 0.6f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	Prison_Watchtower.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(4.5f, 0.0f, 1.5f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fenceGate.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-3.5f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-8.0f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-12.5f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-17.0f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(-21.5f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(19.0f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(14.5f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);

	model = glm::mat4();
	model = glm::translate(model, glm::vec3(10.0f, 0.0f, 3.0f)); // Translate it down a bit so it's at the center of the scene
	//model = glm::rotate(model, 5.0f ,glm::vec3(5.0f, 0.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));	// It's a bit too big for our scene, so scale it down
	shader.SetMat4("model", model);
	fence.Draw(shader);
}

unsigned int planeVAO = 0;
void renderFloor()
{
	unsigned int planeVBO;

	if (planeVAO == 0) {
		// set up vertex data (and buffer(s)) and configure vertex attributes
		float planeVertices[] = {
			// positions            // normals         // texcoords
			25.0f, 0.0f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, 0.0f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
			-25.0f, 0.0f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

			25.0f, 0.0f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, 0.0f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
			25.0f, 0.0f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
		};
		// plane VAO
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
		camera.ProcessKeyboard(UP, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
		camera.ProcessKeyboard(DOWN, (float)deltaTime);

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		camera.Reset();
	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	//pCamera->Reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xPos, double yPos)
{
	if (firstMouse)
	{
		lastX = xPos;
		lastY = yPos;
		firstMouse = false;
	}

	GLfloat xOffset = xPos - lastX;
	GLfloat yOffset = lastY - yPos;  // Reversed since y-coordinates go from bottom to left

	lastX = xPos;
	lastY = yPos;

	camera.ProcessMouseMovement(xOffset, yOffset);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	camera.ProcessMouseScroll((float)yOffset);
}
